import javax.script.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) throws Exception {
        String filePath = "C:\\Users\\ASUS\\Downloads\\question.txt";
        List<String> expressions = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            // Read the lines one at a time
            String line;
            while ((line = reader.readLine()) != null) {
                expressions.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        long startTimeSingle = System.nanoTime();
        List<Integer> resultSingle = calculateExpressionsSingleThreaded(expressions);
        long minSingle = Collections.min(resultSingle);
        long maxSingle = Collections.max(resultSingle);
        long sumSingle = resultSingle.stream().reduce(0, Integer::sum);
        long endTimeSingle = System.nanoTime();
        long singleThreadedTime = endTimeSingle - startTimeSingle;
        System.out.println("Single Thread Time : "+singleThreadedTime);
        System.out.println("Min : " + minSingle);
        System.out.println("Max : " + maxSingle);
        System.out.println("Sum : " + sumSingle);
        System.out.println("---------------------------------------------------------");
        long startTimeMulti = System.nanoTime();
        List<Integer> resultMulti = calculateExpressionsMultiThreaded(expressions);
        long minMulti = Collections.min(resultSingle);
        long maxMulti = Collections.max(resultSingle);
        long sumMulti = resultSingle.stream().reduce(0, Integer::sum);
        long endTimeMulti = System.nanoTime();
        long multiThreadedTime = endTimeMulti - startTimeMulti;
        System.out.println("Multi Thread Time : " + multiThreadedTime);
        System.out.println("Min : " + minMulti);
        System.out.println("Max : " + maxMulti);
        System.out.println("Sum : " + sumMulti);

    }

    public static List<Integer> calculateExpressionsSingleThreaded(List<String> expressions) {
        return expressions.stream()
                .map(Test::calculate)
                .collect(Collectors.toList());
    }

    public static  List<Integer> calculateExpressionsMultiThreaded(List<String> expressions) throws Exception {
        int numThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*2);
        List<Callable<Integer>> tasks = expressions.stream()
                .map(expression -> (Callable<Integer>) () -> calculate(expression))
                .collect(Collectors.toList());

        List<Future<Integer>> futures = executor.invokeAll(tasks);
        executor.shutdown();

        return futures.stream()
                .map(future -> {
                    try {
                        return future.get();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    public static Integer calculate(String s) {
        int sum = 0;
        int sign = 1;
        Stack<Integer> st = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (Character.isDigit(ch)) {
                int val = 0;
                while (i < s.length() && Character.isDigit(s.charAt(i))) {
                    val = val * 10 + (s.charAt(i) - '0');
                    i++;
                }
                i--;
                val = val * sign;
                sign = 1;
                sum += val;
            } else if (ch == '(') {
                st.push(sum);
                st.push(sign);
                sum = 0;
                sign = 1;
            } else if (ch == ')') {
                sum *= st.pop();
                sum += st.pop();
            } else if (ch == '-') {
                sign *= -1;
            }
        }
        return sum;
    }
}
